package john.walker.filter;

/**
 * 调用栈信息过滤器
 *
 * @author 30san
 *
 */
public interface StackTraceFilter {

	/**
	 * 是否过滤该类或方法
	 *
	 * @param className   类名
	 * @param methodName  方法名
	 * @return true表示可以不过滤，false表示过滤
	 */
	public boolean accpet(String className, String methodName) ;

}

package john.walker.spi;

import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverPropertyInfo;
import java.sql.SQLException;
import java.sql.SQLFeatureNotSupportedException;
import java.util.Properties;
import java.util.logging.Logger;

/**
 * 通用驱动
 *
 * @author Johnnie Walker
 *
 */
public class CommonProxyDriver implements Driver {

	private Driver driver ;

	@Override
	public Connection connect(String url, Properties info) throws SQLException {
		if(driver == null) {
			if (url.contains("jdbc:mysql")) {
				this.driver = new MysqlProxyDriver();
			} else if (url.contains("jdbc:oracle")) {
				this.driver = new OracleProxyDriver();
			} else if (url.contains("jdbc:derby")) {
				this.driver = new DerbyProxyDriver();
			}
		}
		return driver.connect(url, info);
	}


	@Override
	public boolean acceptsURL(String url) throws SQLException {
		return this.driver.acceptsURL(url);
	}


	@Override
	public DriverPropertyInfo[] getPropertyInfo(String url, Properties info)
			throws SQLException {
		return this.driver.getPropertyInfo(url, info);
	}


	@Override
	public int getMajorVersion() {
		return this.driver.getMajorVersion();
	}


	@Override
	public int getMinorVersion() {
		return this.driver.getMinorVersion();
	}


	@Override
	public boolean jdbcCompliant() {
		return this.driver.jdbcCompliant();
	}


	@Override
	public Logger getParentLogger() throws SQLFeatureNotSupportedException {
		return this.driver.getParentLogger();
	}

}
